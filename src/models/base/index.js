const fs = require('fs');
const { Pod } = require('kubernetes-models/v1');

module.exports = class BaseModel {
    constructor() {
        ///place for general data
    }

    async execute() {
        throw new Error('execute method didn\'t implement');
    }

    async getOutput() {
        throw new Error('getOutput method didn\'t implement');
    }

    async save(data) {
        return fs.writeFile(
            `PATH_TO_FILE/${this.identity}.json`,
            JSON.stringify(data))
    }

    async getOutput(values) {
     const result = {

     };
        Object.keys(values).forEach(key => {
            
            if (Array.isArray(values[key])) {
                //specific logic for array key
            }

            if (typeof values[key] === 'object' && !Array.isArray(values[key]) && values[key] !== null) {
                //specific logic for object key
            }

            if (typeof values[key] === 'string') {
                //specific logic for string key
            }

            if (Number.isInteger(values[key])) {
                //specific logic for number key
            }

        })
        // Create a new instance
        const pod = new Pod({
        
        });

        // Validate against JSON schema
        pod.validate();

        return pod;
    }

}