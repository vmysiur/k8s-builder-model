module.exports = model => {
  const Model = require(`./${model}`)
  return new Model({} /* place for dependency injection if needed*/)
}
