const BaseModel = require('./base');
const { develop } = require('../values/deployment');

module.exports = class DeploymentModel extends BaseModel {

    constructor() {
        super()
        this.identity = 'deployment';
    }

    async execute() {
        //if needed specific logic you will write here
        return this.getOutput(develop);
    }

 

}