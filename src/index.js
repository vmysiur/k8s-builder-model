const MODELS = ['deployment'];

const build = async modelName => {
    const entity = require('./models')(modelName)
    await entity.execute()
        // .then(async data => entity.save(data))
        .catch(async error => console.log('Execute ERROR', error.message))
}


async function generate(models = []) {

    console.log('models:', models.join(',\n\t'));

    for (const model of models) {
        await build(model)
            .catch(err =>
                console.log('❌ BUILD ERROR - %s'), err);
    }

    console.timeLog(`\n⌛ Generated for model(s): ${models}`);
}

    generate(MODELS)
        .catch((error) => console.error(`❌ GENERATE ERROR ${error}`));

        console.log(process.argv);